<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 *
 * APPLICATION TITLE: Site Backup Plus (web version)
 *
 * ABOUT:     Based on an older concept known as "Kubik-Rubik WordPress Backup".
 *
 * AUTHOR:    (This version and port) - Jim S. Smith
 *
 * COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION
 *
 * LICENSE:   GPL 3.0 (Though, more stringent -> towards FSF's definition of "free software", that is.)
 *
 * VERSION:   1.0.6b, Updated: 2022/11/29
 *
 * DESCRIPTION: A web-script to backup CMS-driven websites and their database(s).
 *     Designed to be run in Apache2 environment, with (optionally) PHP-FPM enabled.
 *
 * USE:   Backup website data and their database(s).
 *
 * REQUIREMENTS:
 *
 * 1.  Should use PHP version 7.3 or newer. ( Not yet tested for PHP 8. ),
 * 2.  PHP-MySQLi library, installed and enabled,
 * 3.  PHP-PGSQL (PostgreSQL) library, installed and enabled, (Coming soon!)
 * 4.  PHP-JSON extension library, installed and enabled,
 * 5.  PHP-OpenSSL function library, installed and enabled,
 * 6.  Able to use the "system()" function.
 * 7.  File-system functions: "file_get_contents()" and "file_put_contents()" must be usable,
 * 8.  Able to use PHP functions "md5_file()" and "hash_file()",
 * 9.  Able to use PHP functions "password_hash()" and "password_verify()",
 *
 * OTHER NOTES: This script should be set with a unique "token" for added security.
 *
 **/

/**	WEB INSTALL/CONFIGURATION SCRIPT.	**/

/*	If we needed to do this, means that the config file was corrupted!	*/
if ( file_exists( $config_file ) ) {
	unlink( $config_file );
	}


/**	Must be a new installation? Start up configuration settings then.	**/

//	Set up default values:
if ( ! isset( $_POST ) || ! count( $_POST ) ) {
	$_POST = [
		'empty-folders'	=>	true,
		'locate-root'	=>	0,
		'max-archives'	=>	3,
		];
	}

if ( ! file_exists( $config_file ) ) {
	if ( ! isset( $_POST['set-config'] ) || $_POST['password1'] !== $_POST['password2'] ) {
		$_POST['password1'] = $_POST['password2'] = '';

		if ( isset( $_POST['admin-email'] ) && preg_match( '/[^a-z0-9.@-]+/', $_POST['admin-email'] ) /*|| ! preg_match( '/([a-z0-9.-]{3,})@([a-z0-9.-]{5,})\.([a-z.]{3,})/', $_POST['admin-email'] )*/ ) {
			unset( $_POST['admin-email'] );
			}

		$_POST = [
			'source'			=>	( isset( $_POST['source'] ) ? $_POST['source'] : '..' ),
			'target'			=>	( isset( $_POST['target'] ) ? $_POST['target'] : '../.backup' ),
			'empty-folders'		=>	( isset( $_POST['empty-folders'] ) ? true : false ),
			'archive-name'		=>	( isset( $_POST['archive-name'] ) ? $_POST['archive-name'] : '' ),
			'overwrite'			=>	( isset( $_POST['overwrite'] ) ? true : false ),
			'exclude-files'		=>	( isset( $_POST['exclude-files'] ) ? $_POST['exclude-files'] : '' ),
			'exclude-folders'	=>	( isset( $_POST['exclude-folders'] ) ? $_POST['exclude-folders'] : '' ),
			'delete-script'		=>	( isset( $_POST['delete-script'] ) ? true : false ),
			'add-drop-table'	=>	( isset( $_POST['add-drop-table'] ) ? true : false ),
			'other-db'			=>	( isset( $_POST['other-db'] ) ? $_POST['other-db'] : '' ),
			'locate-root'		=>	( isset( $_POST['locate-root'] ) ? (int)$_POST['locate-root'] : 0 ),
			'only-db'			=>	( isset( $_POST['only-db'] ) ? true : false ),
			'max-archives'		=>	( isset( $_POST['max-archives'] ) ? (int)$_POST['max-archives'] : 3 ),
			'admin-email'		=>	( isset( $_POST['admin-email'] ) ? $_POST['admin-email'] : '' ),
//			'api-token'			=>	( isset( $_POST['password1'] ) ? $_POST['password1'] : false ),
			];

		header( 'Content-Type: text/html; charset=' . CHARSET );

?><!DOCTYPE html>
<html>
<head>
	<meta name='charset' content='<?php echo CHARSET; ?>'>
	<meta name="generator" content="<?php echo $app_title; ?>">
	<meta name="version" content="<?php echo $version_info; ?>">
	<title><?php echo "$app_title, Version: $version_info"; ?> on: <?php echo $_SERVER['SERVER_NAME']; ?></title>

	<style>
		form { width: 500px; margin: 0 auto; font-size: 0.9875em; }
		table { width: 98.75%; }
		td { vertical-align: top; }
		h1, h2, h3, h4, h5, input[type='submit'], .centered { text-align: center; }
		input[type='text'],input[type='email'],input[type='password'],input[type='number'] { width: 235px; height: 23px; padding: 1px; }
		textarea { width: 250px; height: 60px; padding: 2px; }
		.-help { background-color: #000; color: #eee; border-radius: 5px; }
	</style>
</head>

<body>
	<h1><?php echo "$app_title, Version: $version_info"; ?></h1>
	<h2>on: "<?php echo $_SERVER['SERVER_NAME']; ?>"</h2>
	<hr>
	<h3>Configure <?php echo "$app_title" ?>.</h3>

	<form action='<?php echo $_SERVER['PHP_SELF']; ?>' method='post'>
<?php /*		<input name='this-hash' type='hidden' value=''>
*/ ?>

		<table>
		<caption><h4>Backup Configuration Settings:</h4></caption>

		<tr><td colspan='2'><hr></td></tr>
<?php /*
		<tr><td><b class='-help' title=' The start point for the archive. "." means the folder where the Site Backup Plus script is located. If you uploaded the script into a subfolder of the root, then you have to enter ".." to backup the root! '>&nbsp;?&nbsp;</b>&nbsp;Source:&nbsp;&nbsp;</td><td><input name='source' type='text' value='<?php echo $_POST['source']; ?>'></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>
*/ ?>
		<tr><td><b class='-help' title=' Copy the archive to another folder. - The target_directory is always excluded from the zip process. You should change this option if you want to use the script to backup your website files more often than once! '>&nbsp;?&nbsp;</b>&nbsp;Target:</td><td><input name='target' type='text' value='<?php echo $_POST['target']; ?>'></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Also add empty folders to the archive? '>&nbsp;?&nbsp;</b>&nbsp;Empty Folders:</td><td><input name='empty-folders' type='checkbox' value=true<?php echo ( $_POST['empty-folders'] === true ? " checked" : '' ); ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Set a fixed name for the archive. If false, then the name is generated automatically from the server HOSTNAME and date. If you change this option, then you should either enable the option "overwrite" or change the target directory to avoid zip archives with other backup zip archives. '>&nbsp;?&nbsp;</b>&nbsp;Archive Name:</td><td><input name='archive-name' type='text' value='<?php echo $_POST['archive-name']; ?>'></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Should an archive with the same name be overwritten? If set to false, an alternative name is created automatically with a unique timestamp. If you change this option, then you should change the target directory to avoid zip archives with other backup zip archives. '>&nbsp;?&nbsp;</b>&nbsp;Overwrite:</td><td><input name='overwrite' type='checkbox' value=true<?php echo ( $_POST['overwrite'] === true ? " checked" : '' ); ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Exclude files from being added to the zip archive. You have to enter the full path, relative to the server document-root, to the to be excluded file. Separate each entry with a comma - OR - one per line. (EXAMPLE: "folder1/this-file.txt,folder1/folder2/that-file.jpg") '>&nbsp;?&nbsp;</b>&nbsp;Exclude Files:</td><td><textarea name='exclude-files'><?php echo trim( $_POST['exclude-files'] ); ?></textarea></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Exclude folders from being added to the zip archive. You have to enter the full path to the file. Separate the names with a comma - OR - one per line. (EXAMPLES: "folder1, folder2/folder3") Any folder added here, will also cause all of its contents to be excluded from being added to the backup! - If you use Akeeba Backup, then you should exclude the backup folder: "administrator/components/com_akeeba/backup". '>&nbsp;?&nbsp;</b>&nbsp;Exclude Folders:</td><td><textarea name='exclude-folders'><?php echo trim( $_POST['exclude-folders'] ); ?></textarea></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Delete the Backup Script after the archive was created? '>&nbsp;?&nbsp;</b>&nbsp;Delete Script:</td><td><input name='delete-script' type='checkbox' value=true<?php echo ( $_POST['delete-script'] === true ? " checked" : '' ); ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Should a DROP TABLE statement be added to the sql dump file? This is important if you want to overwrite the exsting database entries while importing the backup dump back into the database. If you want to use the dump file on a new database or delete the tables manually in phpMyAdmin, then you should not change the default setting. '>&nbsp;?&nbsp;</b>&nbsp;Add Drop Table:</td><td><input name='add-drop-table' type='checkbox' value=true<?php echo ( $_POST['add-drop-table'] === true ? " checked" : '' ); ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Backup also other databases in addition to the CMSs database? These databases must have the same connection information like the CMSs database. This option is useful if you use for example Piwik on your website on an additional database. Do not enter the database where the CMS is running! Separate the names with a comma - OR - one per line. (EXAMPLE: "database1, database2") '>&nbsp;?&nbsp;</b>&nbsp;Other Db:</td><td><textarea name='other-db'><?php echo trim( $_POST['other-db'] ); ?></textarea></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Where is the script located in the CMSs installation? How many levels from the root directory? This is important to find the configuration.php for the database connection data. If you place this script in the root -> 0. If you place this script in a subfolder of the root directory -> 1 and so on. '>&nbsp;?&nbsp;</b>&nbsp;Locate Root:</td><td><input name='locate-root' type='number' min=0 max=5 value=<?php echo $_POST['locate-root']; ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' The script can also be told to only backup the whole database. If you only need a database dump, then check this option for "true". '>&nbsp;?&nbsp;</b>&nbsp;Only Db:</td><td><input name='only-db' type='checkbox' value=true<?php echo ( $_POST['only-db'] === true ? " checked" : '' ); ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>

		<tr><td><b class='-help' title=' Do you want to delete older backup files? If yes, then change the number to the maximum number of backup archives at the same time. This option only takes affect if you enable a special folder for the backup files with the option "target". You should use these two options to limit the needed disk space taken up by your archives. '>&nbsp;?&nbsp;</b>&nbsp;Max Archives:</td><td><input name='max-archives' type='number' min=0 max=10 value=<?php echo $_POST['max-archives']; ?>></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>


		<tr><td colspan='2'><h4>Please enter your new credentials here:</h4></td></tr>
		<tr><td><b class='-help' title=' Set an email for the admin. If none is entered, then the system "server-admin" email will be used as the default. '>&nbsp;?&nbsp;</b>&nbsp;Admin Email:</td><td><input name='admin-email' type='email' value='<?php echo $_POST['admin-email']; ?>' placeholder='email@server.tld'></td></tr>

		<tr><td colspan='2'><h5>The next two MUST match exactly!</h5></td></tr>
		<tr><td><b class='-help' title=' Create your new security token. If left blank, backup script will proceed without protection. '>&nbsp;?&nbsp;</b>&nbsp;Security Token:</td><td><input name='password1' type='password' value='' placeholder='my set security token'></td></tr>

		<tr><td colspan='2'>&nbsp;</td></tr>
		<tr><td><b class='-help' title=' THIS must be the same as you entered previously '>&nbsp;?&nbsp;</b>&nbsp;Your Token Again:</td><td><input name='password2' type='password' value='' placeholder='my set security token'></td></tr>

		<tr><td colspan='2'><hr></td></tr>
		<tr><td colspan='2'>&nbsp;</td></tr>
		<tr><td colspan='2' class='centered'><input name='set-config' type='submit' value=' Set Config ' title=' Are we ready to go? '></td></tr>

		</table>
	</form>

	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>

</body>

</html>

<?php
		die();
		}


/**	AFTER CONFIGURATION SETTINGS HAVE BEEN "POSTED", CREATE THE CONFIGURATION FILE.	**/

	else {

		function _clean_up( $_data ) {
			$_data = trim( $_data );

			$_data = str_replace( [ "\r\n", ',', ';', ' ' ], "\n", $_data );
			$_temp = array_map( 'trim', explode( "\n", $_data ) );

			$_temp2 = '';

			foreach ( $_temp as $_item ) {
				if ( ! empty( $_item ) ) {
					$_temp2 .= trim( $_item, '/' ) . ',';
					}
				}

			return rtrim( $_temp2, ',' );
			}


		//require INC_DBs . '/xv-hash-custom.inc.php';
		require INC_DBs . '/php-hashes.inc.php';


		foreach ( [ 'source', 'target', 'empty-folders', 'archive-name', 'exclude-files', 'exclude-folders', 'other-db', 'admin-email' ] as $_key => $_val ) {
			if ( ! empty( $_POST[$_key] ) ) {
				$_POST[$_key] = htmlspecialchars( strip_tags( trim( $_val ) ) );
				}

			else {
				$_POST[$_key] = false;
				}
			}

		$_POST['exclude-files'] = str_replace(
			[ '../', './' ],
			[ '', 'htdocs/' ],
			_clean_up( $_POST['exclude-files'] )
			);

		$_POST['exclude-folders'] = str_replace(
			[ '../', './' ],
			[ '', 'htdocs/' ],
			_clean_up( $_POST['exclude-folders'] )
			);

		$_POST['other-db'] = _clean_up( $_POST['other-db'] );

		$_set = [
			'source'			=>	( ! empty( $_POST['source'] ) ? $_POST['source'] : '..' ),
			'target'			=>	( ! empty( $_POST['target'] ) ? $_POST['target'] : '../.backup' ),
			'empty-folders'		=>	( isset( $_POST['empty-folders'] ) ? true : false ),
			'archive-name'		=>	( ! empty( $_POST['archive-name'] ) ? $_POST['archive-name'] : false ),
			'overwrite'			=>	( isset( $_POST['overwrite'] ) ? true : false ),
			'exclude-files'		=>	( ! empty( $_POST['exclude-files'] ) ? $_POST['exclude-files'] : false ),
			'exclude-folders'	=>	( ! empty( $_POST['exclude-folders'] ) ? $_POST['exclude-folders'] : false ),
			'delete-script'		=>	( isset( $_POST['delete-script'] ) ? true : false ),
			'add-drop-table'	=>	( isset( $_POST['add-drop-table'] ) ? true : false ),
			'other-db'			=>	( ! empty( $_POST['other-db'] ) ? $_POST['other-db'] : false ),
			'locate-root'		=>	( isset( $_POST['locate-root'] ) ? (int)$_POST['locate-root'] : 0 ),
			'only-db'			=>	( isset( $_POST['only-db'] ) ? true : false ),
			'max-archives'		=>	( isset( $_POST['max-archives'] ) ? (int)$_POST['max-archives'] : 3 ),
			'admin-email'		=>	( ! empty( $_POST['admin-email'] ) ? $_POST['admin-email'] : $_SERVER['SERVER_ADMIN'] ),
			'api-token'			=>	( ! empty( $_POST['password1'] ) ? create_hash_string( $_POST['password1'] ) : false ),
			];

		file_put_contents( $config_file, json_encode( $_set ) );
		chmod( $config_file, 0640 );
		}
	}


/**	NOW, START THE MAIN BACKUP APPLICATION SCRIPT.	**/

header( 'Location: /site-backup-plus.php' );
die();

