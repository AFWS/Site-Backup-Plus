**This folder and its contents should NOT be web-accessible!**

Please retain the /".folder-name"/ format (the beginning dot as the first character in the folder name) - so that public-access to it can be more easily blocked with server-configuration settings/directives.

These are the includable, internal-use modules for Site Backup Plus.

