<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 *
 * APPLICATION TITLE: Site Backup Plus (web version)
 *
 * ABOUT:     Based on an older concept known as "Kubik-Rubik WordPress Backup".
 *
 * AUTHOR:    (This version and port) - Jim S. Smith
 *
 * COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION
 *
 * LICENSE:   GPL 3.0 (Though, more stringent -> towards FSF's definition of "free software", that is.)
 *
 * VERSION:   1.0.6b, Updated: 2022/11/29
 *
 * DESCRIPTION: A web-script to backup CMS-driven websites and their database(s).
 *     Designed to be run in Apache2 environment, with (optionally) PHP-FPM enabled.
 *
 * USE:   Backup website data and their database(s).
 *
 * REQUIREMENTS:
 *
 * 1.  Should use PHP version 7.3 or newer. ( Not yet tested for PHP 8. ),
 * 2.  PHP-MySQLi library, installed and enabled,
 * 3.  PHP-PGSQL (PostgreSQL) library, installed and enabled, (Coming soon!)
 * 4.  PHP-JSON extension library, installed and enabled,
 * 5.  PHP-OpenSSL function library, installed and enabled,
 * 6.  Able to use the "system()" function.
 * 7.  File-system functions: "file_get_contents()" and "file_put_contents()" must be usable,
 * 8.  Able to use PHP functions "md5_file()" and "hash_file()",
 * 9.  Able to use PHP functions "password_hash()" and "password_verify()",
 *
 * OTHER NOTES: This script should be set with a unique "token" for added security.
 *
 **/

ini_set( 'memory_limit', '128M' );
//ini_set( 'error_reporting', 0 );
@set_time_limit( 3600 );

$backup_for = false;
$this_DB = false;

$this_DB = ( require INC_DBs . '/sbp-configs.inc.php' );

//require INC_DBs . '/xv-hash-custom.inc.php';
require INC_DBs . '/php-hashes.inc.php';


//	Exlude this backup script and its config file.
if ( $_set['exclude-files'] ) {
	$_set['exclude-files'] .= ',';
	}

//	Do not include this app, nor its config-file in the archive.
$_set['exclude-files'] .= basename( $config_file ) . ",$web_folder/$this_app.php";


//	Exclude these folders (if found) from archive.
if ( $_set['exclude-folders'] ) {
	$_set['exclude-folders'] .= ',';
	}

//	Add in our "do-not-include" folder list.
//$_set['exclude-folders'] .= ".tmp,.cache,downloads,$web_folder/.backup,.sbp-includes," . basename( $_set['target'] );
$_set['exclude-folders'] .= "downloads,$web_folder/.backup,.sbp-includes," . basename( $_set['target'] );

//	Which database system will be used?
$_set['this_DB'] = $this_DB;


//	Get all files and (sub-)folders for the zip archive
function zip_it( $zip, $folder, $_set2, $folder_option = false, $folder_script = false, $name_script = false ) {
	if ( ! file_exists( $folder ) || ! ( $dir = opendir( $folder ) ) ) {
		return false;
		}

	//	Save needed information as variable in the first call of the zip function to avoid unnecessary function calls
	if ( ! $folder_option ) {
		$folder_option = $folder;
		}

	if ( ! $folder_script ) {
		$folder_script = basename( dirname( $_SERVER['SCRIPT_NAME'] ) );
		}

	if ( ! $name_script ) {
		$name_script = basename( $_SERVER['SCRIPT_NAME'] );
		}

	$current_folder_archive = str_replace( "$folder_option/", '', $folder );

	//	Go through the current folder
	while ( $file = readdir( $dir ) ) {
		if ( is_dir( "$folder/$file" ) AND $file != '.' AND $file != '..' ) {
			//	Do not zip the target folder of the backup archives
			if ( ( $current_folder_archive == $folder_script OR $current_folder_archive == $folder_option ) AND $file == $_set2['target'] ) {
				continue;
				}

			if ( ! empty( $_set2['exclude-folders'] ) ) {
				if ( $folder == $folder_option ) {
					if ( in_array( $file, $_set2['exclude-folders'] ) ) {
						continue;
						}
					}

				else {
					if ( in_array( "$current_folder_archive/$file", $_set2['exclude-folders'] ) ) {
						continue;
						}
					}
				}

			if ( ! empty( $_set2['empty-folders'] ) ) {
				if ( $folder == $folder_option ) {
					$zip -> addEmptyDir( $file );
					}

				else {
					$zip -> addEmptyDir( "$current_folder_archive/$file" );
					}
				}

			zip_it( $zip, "$folder/$file", $_set2, $folder_option, $folder_script, $name_script );
			}

		elseif ( is_file( "$folder/$file" ) ) {
			//	Do not zip the backup script or a backup archive with the self defined name
			if ( ( $current_folder_archive == $folder_script OR $current_folder_archive == $folder_option ) AND $file == $name_script OR $file == "{$_set2['archive-name']}.zip" ) {
				continue;
				}

			if ( ! empty( $_set2['exclude-files'] ) ) {
				if ( $folder == $folder_option ) {
					if ( in_array( $file, $_set2['exclude-files'] ) ) {
						continue;
						}
					}

				else {
					if ( in_array( "$current_folder_archive/$file", $_set2['exclude-files'] ) ) {
						continue;
						}
					}
				}

			//	Add the files to the zip archive and set a correct local name
			if ( $folder == $folder_option ) {
				$zip -> addFile( "$folder/$file", $file );
				}

			else {
				$zip -> addFile( "$folder/$file", "$current_folder_archive/$file" );
				}
			}
		}

	closedir( $dir );

	return true;
}

//	Create filename for the archive from the URL and the date
function createFilename( $_set2, $timestamp = false ) {
	if ( empty( $_set2['archive-name'] ) ) {
		$path = pathinfo( $_SERVER['PHP_SELF'] );

		if ( ! empty( $path['dirname'] ) AND $path['dirname'] != '/' ) {
			$url = str_replace( '/', '-', "{$_SERVER['HTTP_HOST']}{$path['dirname']}" );
			}

		else {
			$url = $_SERVER['HTTP_HOST'];
			}

		$date = date( 'Y-m-d' );

		if ( ! $timestamp ) {
			$file_name = $url . '_' . $date . '.zip';
			}

		else {
			$file_name = $url . '_' . $date . '_' . time() . '.zip';
			}
		}

	else {
		if ( ! $timestamp ) {
			$file_name = $_set2['archive-name'] . '.zip';
			}

		else {
			$file_name = $_set2['archive-name'] . '_' . time() . '.zip';
			}
		}

	if ( is_file( "{$_set2['target']}/$file_name" ) ) {
		if ( empty( $_set2['overwrite'] ) ) {
			$file_name = createFilename( $_set2, true );
			}

		else {
			unlink( "{$_set2['target']}/$file_name" );
			}
		}

	return $file_name;
}

//	Get the data from the CMS's database
function getDbData( $host, $user, $pass, $name, $add_drop_table, $file_name, $this_encoding = 'UTF8' ) {
	$link = sbp_connect( $host, $user, stripslashes( $pass ), $name );

//  Only if the database could be connected and the charset could be set to utf8 (otherwise not all characters can be encoded correctly)
	if ( $link and ( $charset = sbp_set_encoding( $link, $this_encoding ) ) ) {
//      Create a temporary, empty dump file. This is required to avoid memory timeouts on large databases!
		touch( $file_name );

		$tables = [];
		$result = sbp_query( $link, 'SHOW TABLES' );

		while ( $row = sbp_fetch_row( $result ) ) {
			$tables[] = $row[0];
			}

		foreach ( $tables as $table ) {
			$data = '';

			$result = sbp_query( $link, "SELECT * FROM $table" );
			$num_fields = sbp_num_fields( $result );
			$count = sbp_num_rows( $result );

			if ( $add_drop_table ) {
				$data .= "DROP TABLE $table ;\n\n";
				}

			$row_create = sbp_fetch_row( sbp_query( $link, "SHOW CREATE TABLE $table" ) );

			$data .= $row_create[1] . ";\n\n";

			if ( $count > 0 ) {
				$data .= "INSERT INTO `$table` VALUES \n";

				$count_entries = 0;

				while ( $row = sbp_fetch_row( $result ) ) {
					$count_entries++;

					$data .= '(';

					for ( $j = 0; $j < $num_fields; $j++ ) {
						//	Prepare data for a correct syntax
						$row[$j] = str_replace( [ "\\", "'" ], [ "\\\\", "''" ], $row[$j] );
						$row[$j] = preg_replace( "@\r\n@", '\r\n', $row[$j] );

						if ( isset( $row[$j] ) ) {
							if ( is_numeric( $row[$j] ) ) {
								$data .= $row[$j];
								}

							else {
								$data .= '\'' . $row[$j] . '\'';
								}
							}

						else {
							$data .= '\'\'';
							}

						if ( $j < ( $num_fields - 1 ) ) {
							$data .= ', ';
							}
						}

					if ( $count_entries < $count ) {
						//	Add a new INSERT INTO statement after every fiftieth entry to avoid timeouts
						if ( $count_entries % 50 == 0 ) {
							$data .= ");\n";
							$data .= "INSERT INTO `$table` VALUES \n";
							}

						else {
							$data .= "),\n";
							}
						}
					}

				$data .= ");\n";
				}

			$data .= "\n\n";

			//	Add the data to the temporary dump file
			file_put_contents( $file_name, $data, FILE_APPEND );
			}

		sbp_close( $link );

		return true;
		}

	return false;
}

//	Create a SQL Dump of the CMS's database and add it directly to the archive
function backupDatabase( $zip, $file_name, $_set2 ) {
	$dump_errors = [];
	$dump_file_names = [];

	if ( $_db = getDbLoginInfo( $_set2['locate-root'] ) ) {

		//	Get the data from the database
		$file_name_dump = str_replace( '.zip', '', "$file_name-{$_db['name']}" ) . '.sql';

		$db_data = getDbData( $_db['host'], $_db['user'], $_db['password'], $_db['name'], $_set2['add-drop-table'], $file_name_dump );

		if ( $db_data ) {
			$zip -> addFile( $file_name_dump );

			$dump_file_names[] = $file_name_dump;
			}

		else {
			$dump_errors[] = "Could not retrieve the {$_set2['archive-name']} Database data";
			}

		if ( ! empty( $_set2['other-db'] ) ) {
			foreach ( $_set2['other-db'] as $additional_database ) {
				$file_name_dump = str_replace( '.zip', '', "$file_name-$additional_database" ) . '.sql';

				//	Get the data from the database
				$db_data = getDbData( $_db['host'], $_db['user'], $_db['password'], $_db['name'], $_set2['add-drop-table'], $file_name_dump );

				if ( $db_data ) {
					$zip -> addFile( $file_name_dump );

					$dump_file_names[] = $file_name_dump;
					}

				else {
					$dump_errors[] = "Could not retrieve the additional database: $additional_database data.";
					}
				}
			}
		}

	else {
		$dump_errors[] = '<i>Could not find the site configuration file! - This Backup Script needs to find the configuration file either in the server "document root", or, is in the case of Wordpress - for example - one level above it</i>';
		}

	$error_output = [ 'error_output' => implode( ', ', $dump_errors ), 'dump_file_names' => $dump_file_names ];

	return $error_output;
}

function timedesc( $a, $b ) {
	return strcmp( $b['last_change'], $a['last_change'] );
}


/* *	Start of main part of the script.	* */

if ( ! file_exists( $_set['target'] ) ) {
	mkdir( $_set['target'], 0740 );
	}

if ( isset( $_POST['get-email'] ) ) {
	$_POST['get-email'] = htmlspecialchars( $_POST['get-email'] );
	}

//	Check whether a token was set and entered correctly
if ( ( ! empty( $_set['api-token'] ) AND isset( $_POST['get-token'] ) AND check_this_hash( $_POST['get-token'], $_set['api-token'] )
	AND $_POST['get-email'] == $_set['admin-email'] ) OR empty( $_set['api-token'] ) ) {

	//	Check whether Zip class exists
	if ( class_exists( 'ZipArchive' ) ) {
		//	Create target directory if not existing
		if ( ! is_dir( $_set['target'] ) ) {
			mkdir( $_set['target'] );
			}

		//	Prepare files to be excluded
		if ( $_set['exclude-files'] ) {
			$_set['exclude-files'] = array_map( 'trim', explode( ',', $_set['exclude-files'] ) );
			}

		//	Prepare folders to be excluded
		if ( $_set['exclude-folders'] ) {
			$_set['exclude-folders'] = array_map( 'trim', explode( ',', $_set['exclude-folders'] ) );
			}

		//	Create name of the new archive
		$file_name = createFilename( $_set );

		//	Create the zip archive
		$zip = new ZipArchive();

		if ( $zip -> open( "{$_set['target']}/$file_name", ZIPARCHIVE::CREATE ) !== true ) {
			$result = false;
			}

		else {
			//	Get all files and folders
			if ( ! $_set['only-db'] ) {
				zip_it( $zip, $_set['source'], $_set );
				}
			}

		//	SQL Dump - Backup the entire database of the CMS website and write it into the archive file
		//	- only if the zip archive could be created.

		//	Prepare other databases to be included in this archive.
		if ( $zip -> status == 0 ) {
			if ( ! empty( $_set['other-db'] ) ) {
				$_set['other-db'] = array_map( 'trim', explode( ',', $_set['other-db'] ) );
				}

			$sql_result = backupDatabase( $zip, $file_name, $_set );

			$sql_result_error = $sql_result['error_output'];
			}

		else {
			$sql_result_error = true;
			}

		$number_of_zipped_files_folders = $zip -> numFiles;

		$zip -> close();

		$status_code = $zip -> status;

		//	Was the zip archive created successfully?
		if ( $status_code == 0 ) {
			$result = true;
			}

		else {
			$result = false;
			}

		//	Delete the temporary database dump files
		if ( ! empty( $sql_result['dump_file_names'] ) ) {
			foreach ( $sql_result['dump_file_names'] as $dump_file_name ) {
				unlink( $dump_file_name );
				}
			}

		$output = '';

		if ( $result && ! $sql_result_error ) {
			$output .= '<h3 class="SUCCESS">SUCCESS!</h3><p>Archive backup is completed. A total of <u>' . $number_of_zipped_files_folders . '</u> files and folders were processed.</p>

			<p>The complete database is also contained in this archive for complete restoration of the site!</p>

			<p class="SUCCESS">The archive file can be found here: <a href="' . "{$_set['target']}/$file_name" . '" title="Zip Archive ' . $file_name . '">' . "{$_set['target']}/$file_name" . '</a></p>

			<p class="SUCCESS">MD5 = ' . md5_file( "{$_set['target']}/$file_name" ) . ' ,</p>

			<p class="SUCCESS">SHA256 = ' . hash_file( 'sha256', "{$_set['target']}/$file_name" ) . '</p>';
			}

		elseif ( $result && $sql_result_error ) {
			$output .= '<h3 class="NOTICE">NOTICE!</h3>

			<p>Archive backup is finished. A total of <u>' . $number_of_zipped_files_folders . '</u> files and folders were processed.</p>

			<p class="EMPHASIZED">Database(s) was/were NOT archived due to either an error or connection problem (and its reported error code is: ' . $sql_result_error . '), or no CMS was identified in use at this site.</p>

			<p class="SUCCESS">However,<br /><br />The archive file can be found here: <i href="' . "{$_set['target']}/$file_name" . '" title="Zip Archive ' . $file_name . '">' . "{$_set['target']}/$file_name" . '</i></p>

			<p class="SUCCESS">MD5 = ' . md5_file( "{$_set['target']}/$file_name" ) . ' ,</p>

			<p class="SUCCESS">SHA256 = ' . hash_file( 'sha256', "{$_set['target']}/$file_name" ) . '</p>';
			}

		else {
			$output .= '<h3 class="ERROR">ERROR!</h3>

			<p>Could not open a ZIP-archive. Reported Error Statuscode: <u>' . $status_code . '</u>.<br />More Information can be found here: <a href="https://www.php.net/manual/en/zip.constants.php#83827" title="Zip Predefined Constants - Failure Codes">PHP Zip Failure Codes</a></p>';
			}

		//	Delete the Backup Script after the backup archive was created?
		if ( $_set['delete-script'] === true ) {
			if ( unlink( basename( $_SERVER['SCRIPT_NAME'] ) ) === true ) {
				$output .= "<h3 class='SUCCESS'>Successfully Deleted - $this_app Backup Script.</h3>";
				}

			else {
				$output .= "<h3 class='ERROR'>Could not delete - $this_app Backup Script!</h3>";
				}
			}

		if ( ! empty( $_set['max-archives'] ) AND $_set['target'] != '.' ) {
			if ( $dir = @opendir( $_set['target'] ) ) {
				$backup_files = array();
				$count = 0;

				while ( $file = readdir ( $dir ) ) {
					if ( is_file( "{$_set['target']}/$file" ) AND $file != '.' AND $file != '..' AND substr( strtolower( $file ), -3 ) == 'zip' ) {
						$backup_files[$count]['name'] = $file;
						$backup_files[$count]['last_change'] = filemtime( "{$_set['target']}/$file" );

						$count++;
						}
					}

				usort( $backup_files, 'timedesc' );

				$number_of_backup_archives = count( $backup_files );

				for ( $i = $_set['max-archives']; $i < $number_of_backup_archives; $i++ ) {
					unlink( "{$_set['target']}/{$backup_files[$i]['name']}" );
					}
				}
			}
		}

	else {
		$output = '<p class="ERROR">The PHP Zip-class library was either not installed, or has not been enabled. For for more information on PHP-Zip-class library: <a href="https://www.php.net/manual/en/book.zip.php" target="_blank" title="Zip Archive on PHP.net">https://www.php.net/manual/en/book.zip.php</a></p>';
		}
	}

else {
	$output = '<h3 class="ERROR">Bad credentials entered!</h3><hr><p class="ERROR">This script is configured to run only when the correct security credentials are provided.</p>';
	}


/**	PREPARE AND SEND AN HTML-FORMATTED PAGE OF THE RESULTS TO THE USER-AGENT.	**/

$script_title = "$app_title Backup Script, V$version_info";

header( 'Content-Type: text/html; charset=' . CHARSET );

?><!DOCTYPE html>
<html>
	<head>
		<meta name="charset" content="<?php echo CHARSET; ?>">
		<meta name="generator" content="<?php echo $app_title; ?>">
		<meta name="version" content="<?php echo $version_info; ?>">
		<title><?php echo $app_title; ?> - Archiving: <?php echo $backup_for; ?></title>
		<style>
			body {
				background: #ddd;
				}

			.CENTERED {
				text-align: center;
				}

			.CONTAINER {
				clear: both;
				width: 725px;
				margin: 0 auto;
				}

			.SUCCESS {
				color: darkgreen;
/*				font-size: 1.10em;	*/
				}

			.NOTICE {
				color: #960;
/*				font-size: 1.5em;	*/
				}

			.EMPHASIZED {
				color: #555;
				font-weight: 700;
				}

			.ERROR {
				color: red;
/*				font-size: 1.5em;	*/
				}
		</style>
	</head>
	<body>
		<h1 class="CENTERED"><?php echo "$app_title, Version: $version_info"; ?></h1>
		<h2 class="CENTERED">Archived: <?php echo $backup_for; ?></h2>
		<hr>
		<h3 class="CENTERED">Operation Result:</h3>
		<hr>

		<div class="CONTAINER">
		<?php echo $output; ?>
		</div>

		<div class="CONTAINER">
		</div>

	</body>
</html>

<?php

die();

