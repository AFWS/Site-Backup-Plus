<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 *
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Requires:        Ability to execute PHP functions "password_hash()", "password_verify()" and "openssl_random_pseudo_bytes()".
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 **/

/**	HASH-GENERATION MODULE, ADAPTIVE.	**/

defined( 'STR_STACK' ) || define( 'STR_STACK', implode( '', array_merge( [ '.', '/' ], range( '0', '9' ), range( 'A', 'Z' ), range( 'a', 'z' ) ) ) );


/*	USES: PHP built-in password-hash functions.	*/

function _get_hash_cost( $_starter = 8 ) {
	$timeTarget = 0.05; // 50 milliseconds
	$cost = $_starter;

	do {
		$cost++;
		$start = microtime( true );
		password_hash( "test", PASSWORD_BCRYPT, [ "cost" => $cost ] );
		$end = microtime( true );
	} while ( ( $end - $start ) < $timeTarget );

	return (int) $cost;
}

function _random_str( $count = 8 ) {
	if ( $count < 8 ) {
		 $count = 8;
		 }

	if ( function_exists( 'openssl_random_pseudo_bytes' ) ) {
		$_seed = openssl_random_pseudo_bytes( $count );
		}

	else {
		$_seed = file_get_contents( '/dev/urandom', false, null, 0, $count );
		}

	return _encode64( $_seed );
}

function _encode64( $input ) {
	$output = '';
	$i = 0;
	$count = strlen( $input );

	do {
		$value = ord( $input[$i++] );
		$output .= STR_STACK[$value & 0x3f];

		if ( $i < $count )
			$value |= ord( $input[$i] ) << 8;

		$output .= STR_STACK[( $value >> 6 ) & 0x3f];

		if ( $i++ >= $count )
			break;

		if ( $i < $count )
			$value |= ord( $input[$i] ) << 16;

		$output .= STR_STACK[( $value >> 12) & 0x3f];

		if ( $i++ >= $count )
			break;

		$output .= STR_STACK[( $value >> 18 ) & 0x3f];

	} while ( $i < $count );

	return $output;
}


/**	These next two functions are what one would use in an application.	**/

function create_hash_string( $password, $roll = 8, $count = 19 ) {
	if ( empty( $password ) || strlen( $password ) < 8 ) {
		return false;
		}

	if ( defined( 'PASSWORD_ARGON2ID' ) ) {
		return password_hash( $password, PASSWORD_ARGON2ID );
		}

	elseif ( defined( 'PASSWORD_ARGON2I' ) ) {
		return password_hash( $password, PASSWORD_ARGON2I );
		}

	elseif ( defined( 'PASSWORD_BCRYPT' ) ) {
		$options = [
			'cost' => _get_hash_cost( $roll ),
			];

		return password_hash( $password, PASSWORD_BCRYPT, $options );
		}

	else {
		$_salt = substr( _encode64( _random_str( $count * 2 ) ), 0, 16 );
		$_cost = _get_hash_cost( $roll );

		return crypt( $password, '$6$rounds=' . ( $_cost * 1000 ) . '$' . $_salt . '$' );
		}

	return false;
}

function check_this_hash( $password, $this_hash ) {
	if ( empty( $password ) || empty( $this_hash ) ) {
		return false;
		}

	return ( password_verify( $password, $this_hash ) === true );
}

