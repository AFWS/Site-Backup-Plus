<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

$backup_for = 'PHPbb3';

function getDbLoginInfo( $located_root = 0 ) {
	// Get the database data from the configuration file
	if ( file_exists( './config.php' ) && ( $conf_data = file_get_contents( './config.php' ) ) ) {
		if ( preg_match( '!(@define\(\'PHPBB_INSTALLED\',\ true\);)!', $conf_data ) ) {
			preg_match( '@\$dbname\h*=\h*\'(.*)\';@U',		$conf_data, $db );
			preg_match( '@\$dbuser\h*=\h*\'(.*)\';@U',		$conf_data, $user );
			preg_match( '@\$dbpasswd\h*=\h*\'(.*)\';@U',	$conf_data, $password );
			preg_match( '@\$dbhost\h*=\h*\'(.*)\';@U',		$conf_data, $host );

			preg_match( '@\$dbms\h*=\h*\'(.*)\';@U',	$conf_data, $this_db );

			$this_db[1] = explode( '\\\\', $this_db[1] ); // YUCK! - A very messy way to find out which DB server is being used!
			}

		else {
			return false;
			}
		}

	else {
		return false;
		}

	return [ 'name' => $db[1], 'user' => $user[1], 'password' => $password[1], 'host' => $host[1], 'dbms' => $this_db[1][3] ];
}

