<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

$backup_for = 'Drupal7';

function getDbLoginInfo( $located_root = false ) {
	// Get the database data from the configuration file
	if ( file_exists( './sites/default/settings.php' ) && ( $conf_data = file_get_contents( './sites/default/settings.php' ) ) ) {
		if ( preg_match( '@(\*\ Drupal\ site-specific\ configuration\ file\.)@', $conf_data ) ) {
			preg_match( '!\*/\s*\$databases\s*=\s*array\s*\((.*)\);\s+/\*+!sU', $conf_data, $d );

			preg_match( '@\'database\'\h*=>\h*\'(.*)\',@U',		$d[0], $db );
			preg_match( '@\'username\'\h*=>\h*\'(.*)\',@U',		$d[0], $user );
			preg_match( '@\'password\'\h*=>\h*\'(.*)\',@U',		$d[0], $password );
			preg_match( '@\'host\'\h*=>\h*\'(.*)\',@U',			$d[0], $host );

			preg_match( '@\'driver\'\h*=>\h*\'(.*)\',@U',		$d[0], $this_db );
			}

		else {
			return false;
			}
		}

	else {
		return false;
		}

	return [ 'name' => $db[1], 'user' => $user[1], 'password' => $password[1], 'host' => $host[1], 'dbms' => $this_db[1] ];
}

