<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

$backup_for = 'B2Evolution';

function getDbLoginInfo( $located_root = 0 ) {
	// Get the database data from the configuration file
	if ( file_exists( './conf/_basic_config.php' ) && ( $conf_data = file_get_contents( './conf/_basic_config.php' ) ) ) {
		if ( preg_match( '@(\$db_config\h*=\h*array\((.*)\);)@sU', $conf_data ) ) {
			preg_match( '@\'name\'\h*=>\h*\'(.*)\',@U',		$conf_data, $db );
			preg_match( '@\'user\'\h*=>\h*\'(.*)\',@U',		$conf_data, $user );
			preg_match( '@\'password\'\h*=>\h*\'(.*)\',@U',	$conf_data, $password );
			preg_match( '@\'host\'\h*=>\h*\'(.*)\',@U',		$conf_data, $host );
			}

		else {
			return false;
			}
		}

	else {
		return false;
		}

	return [ 'name' => $db[1], 'user' => $user[1], 'password' => $password[1], 'host' => $host[1], 'dbms' => 'mysql' ];
}

