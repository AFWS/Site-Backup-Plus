<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */


/* *	DB-discovery functions for supported CMS's.	* */

if ( ! function_exists( 'getDbLoginInfo' ) ) {

//	Attempt to determine which CMS is installed.

/*	WordPress/ClassicPress?	*/
	if ( file_exists( './wp-login.php' ) && file_exists( './wp-admin' ) && file_exists( './wp-content' ) && file_exists( './wp-includes' ) ) {
		require __DIR__ . '/cms-specific/wp.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	Drupal7?	*/
	elseif ( file_exists( './index.php' ) && strpos( file_get_contents( './index.php' ), "define('DRUPAL_ROOT'," ) !== false ) {
		require __DIR__ . '/cms-specific/d7.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	Joomla?	*/
	elseif ( file_exists( './index.php' ) && strpos( file_get_contents( './index.php' ), "define('JOOMLA_MINIMUM_PHP'," ) !== false ) {
		require __DIR__ . '/cms-specific/joom.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	PHPBB?	*/
	elseif ( file_exists( './index.php' ) && strpos( file_get_contents( './index.php' ), "define('IN_PHPBB', true);" ) !== false ) {
		require __DIR__ . '/cms-specific/phpbb.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	B2Evolution?	*/
	elseif ( file_exists( './evoadm.php' ) && file_exists( './admin.php' ) && strpos( file_get_contents( './admin.php' ), "require_once 'evoadm.php';" ) !== false ) {
		require __DIR__ . '/cms-specific/b2.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	PHPMyAdmin?	*/
	elseif ( file_exists( './index.php' ) && strpos( file_get_contents( './index.php' ), 'use PhpMyAdmin\Routing;' ) !== false ) {
		require __DIR__ . '/cms-specific/pma.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}

/*	MediaWiki?	*/
	elseif ( file_exists( './LocalSettings.php' ) && strpos( file_get_contents( './LocalSettings.php' ), 'defined( \'MEDIAWIKI\' )' ) !== false ) {
		require __DIR__ . '/cms-specific/mwiki.inc.php';
		$this_DB = getDbLoginInfo()['dbms'];
		}


/*	NONE compatible found!	*/
	else {
		header( 'X-Warning: No usable database server found!' );
//		die( '<h1>ATTENTION!:</h1><p>Could not find out which CMS is used to render this site!</p><p>This CMS is probably not yet supported.</p>' );

		function getDbLoginInfo( $located_root = false ) {
			return false;
			}
		}
	}

if ( ! $backup_for ) {
	$backup_for = $_SERVER['SERVER_NAME'];
	}

else {
	$backup_for .= " on: {$_SERVER['SERVER_NAME']}";
	}

if ( $this_DB ) {
	require INC_DBs . "/db-drivers/{$this_DB}-func.inc.php";
	}

else {
	require INC_DBs . '/db-drivers/default-db-func.inc.php';
	}


return $this_DB;

