<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

/* *	Other SQL-like functions.	* */

/*
	$link = ???_connect( "host=$host user=$user password=" . stripslashes( $pass ) . " dbname=$name" );
	???_set_client_encoding( $link, 'UTF8' );
	???_query( $link, 'SHOW TABLES' );
	???_fetch_row( $result )
	???_num_fields( $result );
	???_num_rows( $result );
	???_close( $link );
*/

/* *	Other SQL-like functional-shims.	* */

function sbp_connect( $host, $user, $pass, $name ) {
//	return ???_connect( "host=$host user=$user password=$pass dbname=$name" );
	return false;
}

function sbp_set_encoding( $res, $param = 'UTF8' ) {
//	return ???_set_client_encoding( $res, strtoupper( $param ) );
	return false;
}

function sbp_query( $res, $query_param ) {
//	return ???_query( $res, $query_param );
	return false;
}

function sbp_fetch_row( $io_result ) {
//	return ???_fetch_row( $io_result );
	return false;
}

function sbp_num_fields( $io_result ) {
//	return ???_num_fields( $io_result );
	return false;
}

function sbp_num_rows( $io_result ) {
//	return ???_num_rows( $io_result );
	return false;
}

function sbp_close( $res ) {
//	return ???_close( $res );
	return false;
}


/* *	"NOT PROVISIONED"-message.	* */

//die( "<!DOCTYPE html><html><h1>DATABASE NOT PROVISIONED YET!</h1><hr><p>THIS is a provisional dummy-package for those instances where no known database server is found to be used yet.</p></html>" );

