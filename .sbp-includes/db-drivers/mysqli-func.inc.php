<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

include __DIR__ . '/mysql-func.inc.php';
