<?php	defined( 'SBP_CMS_BACKUP' ) || die();
/**
 * Name:            SITE BACKUP PLUS
 * Version:         1.0.6b for any supported CMS which uses MySQL database.
 * Author:          Jim S. Smith
 * Website:         http://afreshwebsolution.com
 *
 * License:         GPLv3
 *
 * Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump) within seconds for your Wordpress website. The default settings are optimized for one zip process from the root folder. Please adjust the settings if you want to use this script to create backups more often than once!
 *
 * Edited/Updated:  J.S.Smith, AFWS
 *
 */

/**	MySQL functions.	**/

/*
	mysqli_connect( $host, $user, stripslashes( $pass ), $name );
	mysqli_set_charset( $link, 'utf8' )
	mysqli_query( $link, 'SHOW TABLES' );
	mysqli_fetch_row( $result )
	mysqli_num_fields( $result );
	mysqli_num_rows( $result );
	mysqli_close( $link );
*/

/**	MySQL functional-shims.	**/

function sbp_connect( $host, $user, $pass, $name ) {
	return mysqli_connect( $host, $user, $pass, $name );
}

function sbp_set_encoding( $res, $param = 'utf8' ) {
	return mysqli_set_charset( $res, strtolower( $param ) );
}

function sbp_query( $res, $query_param ) {
	return mysqli_query( $res, $query_param );
}

function sbp_fetch_row( $io_result ) {
	return mysqli_fetch_row( $io_result );
}

function sbp_num_fields( $io_result ) {
	return mysqli_num_fields( $io_result );
}

function sbp_num_rows( $io_result ) {
	return mysqli_num_rows( $io_result );
}

function sbp_close( $res ) {
	return mysqli_close( $res );
}
