<?php	defined( 'SBP_CMS_BACKUP' ) || define( 'SBP_CMS_BACKUP', true );
/**
#    APPLICATION TITLE: Site Backup Plus (web version)
 
   **ABOUT:**     Based on an older concept known as "Kubik-Rubik WordPress Backup".
 
   **AUTHOR:**    (This version and port) - Jim S. Smith
 
   **COPYRIGHT:** (c)2022 - A FRESH WEB SOLUTION
 
   **LICENSE:**   GPL 3.0 (Though, more stringent -> towards FSF's definition of "free software", that is.)
 
   **VERSION:**   1.0.6b, Updated: 2022/11/29
 
   **DESCRIPTION:** A web-script to backup CMS-driven websites and their database(s).
       Designed to be run in Apache2 environment, with (optionally) PHP-FPM enabled.
 
   **USE:**   Backup website data and their database(s).
 
   REQUIREMENTS:
 
   1.  Should use PHP version 7.3 or newer. ( Not yet tested for PHP 8. ),
   2.  PHP-MySQLi library, installed and enabled,
   3.  PHP-PGSQL (PostgreSQL) library, installed and enabled, (Coming soon!)
   4.  PHP-JSON extension library, installed and enabled,
   5.  PHP-OpenSSL function library, installed and enabled,
   6.  Able to use the "system()" function.
   7.  File-system functions: "file_get_contents()" and "file_put_contents()" must be usable,
   8.  Able to use PHP functions "md5_file()" and "hash_file()",
   9.  Able to use PHP functions "password_hash()" and "password_verify()",
 
   **OTHER NOTES:** This script should be set with a unique "token" for added security.

 **/

defined( 'INC_DBs' ) || define( 'INC_DBs', dirname( $_SERVER['DOCUMENT_ROOT'] ) . '/.sbp-includes' );


//	DEAL WITH INJECTION-THREATS.
foreach ( [ 'SERVER_NAME', 'DOCUMENT_ROOT', 'SCRIPT_FILENAME', 'PHP_SELF', 'SERVER_ADMIN' ] as $this_var ) {
	$_SERVER[$this_var] = htmlspecialchars( strip_tags( $_SERVER[$this_var] ) );
	}


//$this_app = strstr( basename( $_SERVER['SCRIPT_FILENAME'] ), '.', true );
$this_app = 'site-backup-plus';
$web_folder = basename( $_SERVER['DOCUMENT_ROOT'] );
$version_info = '1.0.6b';
$app_title = ucwords( str_replace( '-', ' ', $this_app ) );


//	OTHER DEFINES AND VARIABLES.
define( 'CHARSET', @ini_get( 'default_charset' ) );
define( 'TEMP_PATH', ( ! empty( ini_get( 'upload_tmp_dir' ) ) ? ini_get( 'upload_tmp_dir' ) : sys_get_temp_dir() ) );


//	Set some browser-security headers.
header( "X-Content-Type-Options: nosniff" );
header( "X-XSS-Protection: 1; mode=block" );
header( "Referrer-Policy: no-referrer, strict-origin-when-cross-origin" );
header( "Permissions-Policy: microphone=(), camera=(), vibrate=(), geolocation=(), payment=(), sync-xhr=(), interest-cohort=()" );
header( "X-Permitted-Cross-Domain-Policies: 'none'" );
header( "Content-Security-Policy: base-uri 'self'; default-src 'none'; script-src 'none'; style-src 'self' 'unsafe-inline'; object-src 'none'; frame-src 'none'; reflected-xss filter; form-action 'self' {$_SERVER['SERVER_NAME']}");


$config_file = "../$this_app.cnf.json";

if ( ! file_exists( $config_file ) OR ! ( $_set = json_decode( file_get_contents( $config_file ), true ) ) ) {
	include INC_DBs . '/sbp-web-install.inc.php';
	die();
	}


if ( ! $_set['api-token'] || ( $_set['api-token'] && isset( $_POST['backup-site'] ) ) ) {
	include INC_DBs . '/sbp-main.inc.php';
	die();
	}


/*	If "token" was set, need to display the authorization form first.	*/

header( 'Content-Type: text/html; charset=' . CHARSET );

?><!DOCTYPE html>
<html>
<head>
	<meta name='charset' content='<?php echo CHARSET; ?>'>
	<meta name="generator" content="<?php echo $app_title; ?>">
	<meta name="version" content="<?php echo $version_info; ?>">
	<title><?php echo "$app_title, Version: $version_info"; ?> on: <?php echo $_SERVER['SERVER_NAME']; ?></title>

	<style>
		form { width: 300px; margin: 0 auto; }
		table { width: 98%; }
		h1, h2, h3, h4, input[type='submit'], .centered { text-align: center; }

	</style>
</head>

<body>
	<h1><?php echo "$app_title, Version: $version_info"; ?></h1>
	<h2>on: "<?php echo htmlspecialchars( $_SERVER['SERVER_NAME'] ); ?>"</h2>
	<hr>
	<h3>Ready to backup your site.</h3>

	<form action='<?php echo htmlspecialchars( $_SERVER['PHP_SELF'] ); ?>' method='post'>
<?php /*		<input name='this-hash' type='hidden' value=''>
*/ ?>

		<table>
		<caption><h4>Please enter your authentication for this action.</h4></caption>

		<tr><td colspan='2'><hr></td></tr>
		<tr><td>&nbsp;Email:&nbsp;</td><td><input name='get-email' type='email' value='' placeholder='email@server.tld' required title=' The same email that was set in the configuration file. These must match! '></td></tr>
		<tr><td>&nbsp;Token:&nbsp;</td><td><input name='get-token' type='password' value='' required placeholder='my set security token' title=' A token was set. Therefore, you need to enter it before any backup operation takes place. '></td></tr>
		<tr><td colspan='2'><hr></td></tr>
		<tr><td colspan='2' class='centered'><input name='backup-site' type='submit' value='Backup Site' title=' Are we ready to go? '></td></tr>

		</table>
	</form>
</body>

</html>
