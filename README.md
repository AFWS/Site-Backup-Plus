# Site Backup Plus

## Version: 1.0.6b

**A convenient web-accessible website archiving solution, based on an older project: "Kubik-Rubik Wordpress Backup Script" - but with a few additional features.**

This project is uploaded for resurrecting the older project which appeared to be discontinued, and is no longer present on the web. I felt that this utility was too useful to let disappear and be forgotten, as there are many websites which could use a very useful and much-needed utility such as this one.

Therefore,

*Site Backup Plus is my own contribution to the web, and for website operators/owners in general.*

To install this script (and its necessary libraries),

**A shell-script has been provided if the shell environment is accessible.**

If THAT is not useable in your situation, then simply copy/upload the main script, "*site-backup-plus.php*"
to the main "document root" folder (the one that is accessible to the public - and contains your CMS and other site resources), and the library folder, "*.sbp-includes*", to one level above the document root (if permitted by your web-hosting service). Then, the first start up of the script will present a configuration/settings page, where one can determine how and what the backup script will archive (*as well as secure it with a password*).

<< That is all one should need to do in order for this backup script to start working for archiving a website. >>

*It is HIGHLY suggested that you go ahead and set a "token" key (aka: "password") - at the install/configure screen.*
(*This is where you are asked to enter your new credentials.*)

- This will add some additional security - so that others will not have the ability to just arbitrarily download
a complete copy of your website (*plus any of the databases too*).

Any password "stored" in the configuration file, is first "hashed", using PHP's one-way password-hashing functions - much in the same manner that most of the established CMS's do. **NO PLAIN-TEXT PASSWORD (OR "TOKEN") IS EVER STORED IN THE COFIGURATION SETTINGS FILE!** Only the password hash.

Once set up,

Site Backup Plus is ready and waiting for whenever you need to make a "snapshot" backup of your website's contents.


**FOR ADDED SECURITY:**

You should set the file permissions bits for ".sbp-includes"-folder AND the config file (the one ending in ".cnf.json"
and located just above the "Server Document-Root") to: "0750". (In other words, this setting disallows "world-access" to
these files and folders.)

If you need to reconfigure Site Backup Plus at any time,

All that is needed, is delete the configuration file (in this case, the default name is: "*site-backup-plus.cnf.json*"), and then navigate to the script utility's URL path - to see the familiar "configuration Page".

**CMS's CURRENTLY SUPPORTED:**

* WordPress/ClassicPress,
* PHPBB,
* Drupal7,
* B2Evolution,
* Joomla,
* PHPMyAdmin,
* MediaWiki, (Added: 2022-12-05)

**DATABASE SERVERS CURRENTLY SUPPORTED:**

* MySQL,

All other info about this application script can be found in the manual.

