#!/bin/bash

# # #
#
#  Name:            SITE BACKUP PLUS - Installer Shellscript
#  Version:         1.0.5b for any supported CMS which uses MySQL database.
#  Author:          Jim S. Smith
#  Website:         http://afreshwebsolution.com
#
#  License:         GPLv3
#
#  Description:     With this script you can create backup archives with all files and folders and the whole database (SQL dump)
#                   within seconds for your CMS's website. The default settings are optimized for one zip process from the root folder.
#                   Please adjust the settings if you want to use this script to create backups more often than once!
#
#  Edited/Updated:  J.S. Smith, AFWS
#
# # #


# A NEW WAY TO HANDLE SWITCHING TO 'ROOT' USER.
if [ $(id -u) -ne 0 ] ; then
    _SD='sudo '
else
    _SD=''
fi


_USER=$(id -un)
_GROUP=$(id -gn)


usage () {
	cat <<-_TEXT_

    Site Backup Plus - For archiving your CMS-driven websites.
        Can work with several supported CMS's, AND supports MySQL/MariaDB database servers.

        What's more,

        Can even detect which CMS and what database server it is using (IF the CMS, itself, supports the user's choice
            of database server to use). This is all done by scanning the CMS's "config" files for the database login
            credentials.

    This project was inspired by the discontinued(?) standalone "Kubic-Rubik Wordpress Backup Script".
        [ - Which seems to have disappeared from the internet nowadays. :-(( - ]

    USAGE:

    $(basename $0) <destination_path>   [the "document-root" folder - inside of "htdocs", "public_html", or whichever]

    EXAMPLE: $(basename $0) /home/website_user/document_root/html

_TEXT_

	exit
}


if [ $# -lt 1 ] ; then
    usage
    exit 1
fi

#	Can install copies to multiple web-folders (virtual hosts).
for _P in $1 $2 $3 $4 $5 $6 $7 $8 $9 ; do
	if [ "x$_P" != 'x' ] ; then
        echo -e "Copying to: '$_P'\n\n"

		${_SD}cp ./site-backup-plus.php $_P
		${_SD}cp -R ./.sbp-includes $(dirname $_P)
		${_SD}mkdir $(dirname $_P)/.backup

		${_SD}chmod -R 0750 $(dirname $_P)/.sbp-includes
		${_SD}chmod 0740 $(dirname $_P)/.backup
		${_SD}chmod 0755 $_P/site-backup-plus.php

		${_SD}chown -R "$_USER:$_GROUP" $(dirname $_P)/.sbp-includes
		${_SD}chown "$_USER:$_GROUP" $(dirname $_P)/.backup
		${_SD}chown "$_USER:$_GROUP" $_P/site-backup-plus.php

	else
		exit
	fi
done

